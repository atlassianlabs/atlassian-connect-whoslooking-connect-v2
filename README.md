# Who's Looking v2 JIRA Connect add-on

This is remake of Robin Fernandez' [Who's Looking (Connect)](https://bitbucket.org/atlassianlabs/whoslooking-connect) add-on.

The functionality is the same, however this version is a static HTML page. Yep, you read that correctly. It uses the awesome [Firebase](http://firebase.com) service to do the real-time presence detection entirely on the client.
